package com.test.jpa;

import java.util.List;
import javax.persistence.EntityManager;

import com.test.jpa.model.PurchaseOrder;
import org.apache.camel.component.jpa.JpaEndpoint;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.spring.CamelSpringTestSupport;
import org.junit.Test;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class JpaTest extends CamelSpringTestSupport {

    @Test
    public void testRouteJpa() throws Exception {
        MockEndpoint mock = getMockEndpoint("mock:result");
        mock.expectedMessageCount(1);

        PurchaseOrder purchaseOrder = new PurchaseOrder();
        purchaseOrder.setOrderId(1);
        purchaseOrder.setName("motor");
        purchaseOrder.setAmount(1);
        purchaseOrder.setCustomer("honda");

        template.sendBody("direct:start", purchaseOrder);

        assertMockEndpointsSatisfied();
        assertEntityInDB();
    }

    @Override
    protected AbstractApplicationContext createApplicationContext() {
        return new ClassPathXmlApplicationContext("META-INF/spring/camel-context.xml");
    }


    private void assertEntityInDB() throws Exception {
        JpaEndpoint endpoint = context.getEndpoint("jpa://com.test.jpa.model.PurchaseOrder", JpaEndpoint.class);
        EntityManager em = endpoint.getEntityManagerFactory().createEntityManager();

        List list = em.createQuery("select x from com.test.jpa.model.PurchaseOrder x").getResultList();
        assertEquals(1, list.size());

        assertIsInstanceOf(PurchaseOrder.class, list.get(0));

        em.close();
    }
}
