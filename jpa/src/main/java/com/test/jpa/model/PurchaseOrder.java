package com.test.jpa.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class PurchaseOrder implements Serializable {
    
	private static final long serialVersionUID = 414632787969243627L;

    @Id
	private Integer orderId;
	private String name;
    private double amount;
    private String customer;

    public PurchaseOrder() {
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getCustomer() {
        return customer;
    }

    @Override
    public String toString(){
        return "PurchaseOrder{" +
                "orderId=" + orderId +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                ", customer='" + customer + '\'' +
                '}';

    }
}
