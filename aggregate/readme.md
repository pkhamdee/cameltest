TestRoute
1. fix multicase to make routes to direct:getbanana,direct:getSrawberry
2. fix recipientList, aggregator will produce object FoodColor { color : "red,yello" }  in body
3. add split() for FoodColor.color with tokenize of ","
.split()
.choice()
    .when(simple("${header.xxx} == 'red'"))
        .log("found strawberry")
    .when(simple("${header.xxx} == 'yello'"))
        .log("found banana")
.end()

RouteTest1
3. Test case, add more assert to verify body contents