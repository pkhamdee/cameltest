package com.test.bean;

import org.apache.camel.Exchange;

public class MessageBean {
    public void printExchangeMsg(Exchange exchange) throws Exception{

        System.out.println("exchange properties --> " + exchange.getProperties());
        System.out.println("in.header ---> " + exchange.getIn().getHeaders());
        System.out.println("in.body ---> " + exchange.getIn().getBody());
    }
}
