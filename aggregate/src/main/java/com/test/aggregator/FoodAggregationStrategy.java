package com.test.aggregator;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FoodAggregationStrategy implements AggregationStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(FoodAggregationStrategy.class);

    public Exchange aggregate(Exchange baseExchange, Exchange deltaExchange) {

        return baseExchange;
    }
}
