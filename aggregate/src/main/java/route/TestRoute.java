package route;

import com.test.aggregator.FoodAggregationStrategy;
import com.test.bean.MessageBean;
import com.test.bean.MessageProcessor;
import com.test.model.Banana;
import com.test.model.Strawberry;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;


public class TestRoute extends RouteBuilder {

    private MessageProcessor messageProcessor;
    private MessageBean messageBean;

    @Override
    public void configure() throws Exception {
        from("direct:start")
                .routeId("camelroute-route")
                .recipientList(simple("direct:getbanana,direct:getSrawberry")).aggregationStrategy(new FoodAggregationStrategy()).parallelProcessing().stopOnException().end()


        from("direct:getbanana")
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        Banana banana = new Banana();
                        banana.setColor("yello");
                        exchange.getIn().setBody(banana);
                    }
                });

        from("direct:getSrawberry")
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        Strawberry strawberry = new Strawberry();
                        strawberry.setColour("red");
                        exchange.getIn().setBody(strawberry);
                    }
                });


        from("direct:start2")
                .routeId("camelroute-route")
                .multicast()

    }

    public void setMessageBean(MessageBean messageBean) {
        this.messageBean = messageBean;
    }

    public void setMessageProcessor(MessageProcessor messageProcessor) {
        this.messageProcessor = messageProcessor;
    }
}
