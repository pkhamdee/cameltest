package com.test.route.builder;

import org.apache.camel.Exchange;
import org.apache.camel.Header;
import com.test.route.model.Stocktrading;
import com.test.route.common.ResponseWrapper;
import com.test.route.common.Constant;

public class StockTradingResponse {
    public void buildGetContractStatusResponse(@Header("versionNo") String versionNo,
                                               @Header("serviceFunction") String serviceFunction,
                                               Exchange exchange){

        Stocktrading stocktrading = exchange.getIn().getBody(Stocktrading.class);

        ResponseWrapper responseWrapper = populateResponseBody(stocktrading);

        exchange.getIn().setBody(responseWrapper);
    }

    private ResponseWrapper populateResponseBody(Stocktrading stocktrading)
    {
        ResponseWrapper response = new ResponseWrapper();
        response.setBody(stocktrading, false);
        response.setErrorCode(Constant.RESPONSE_CODE_SUCCESS);

        return response;
    }
}
