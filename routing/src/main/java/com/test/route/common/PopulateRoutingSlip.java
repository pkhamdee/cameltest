package com.test.route.common;

import org.apache.camel.Exchange;
import org.apache.camel.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PopulateRoutingSlip {
    private String pathPropertiesDeliminater = ".";
    private static Logger logger = LoggerFactory.getLogger(PopulateRoutingSlip.class);


    public void genAPIRoutingSlip(@Header("versionNo") String versionNo,
                                  @Header("serviceName") String serviceName,
                                  @Header("serviceFunction") String serviceFunction,
                                  Exchange exchange) throws Exception
    {
        setRoutingPath(versionNo, serviceName, serviceFunction, exchange);
    }

    private void setRoutingPath(String versionNo,
                                String serviceName,
                                String serviceFunction,
                                Exchange exchange) throws Exception {

        if(isBlank(versionNo)){
            throw new Exception("versionNo parameter is required");
        }
        if(isBlank(serviceName)){
            throw new Exception("serviceName parameter is required");
        }
        if(isBlank(versionNo)){
            throw new Exception("versionNo parameter is required");
        }

        String routePropertyPath = serviceName.toUpperCase() + pathPropertiesDeliminater + serviceFunction.toUpperCase() + pathPropertiesDeliminater + versionNo.toUpperCase() +  pathPropertiesDeliminater + "PATH";

        logger.info("routePropertyPath:"+routePropertyPath);

        String apiPath = (String) exchange.getContext().resolvePropertyPlaceholders("{{" + routePropertyPath + "}}");
        logger.info("apiPath:"+routePropertyPath);

        exchange.setProperty("VERSION_NO", versionNo);
        exchange.setProperty("SERVICE_FUNCTION", serviceFunction);
        exchange.setProperty("ROUTING_PATH",  apiPath);
    }

    public boolean isBlank(String str) {
        int strLen;
        if (str == null || (strLen = str.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if ((Character.isWhitespace(str.charAt(i)) == false)) {
                return false;
            }
        }
        return true;
    }
}
