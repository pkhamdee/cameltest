package com.test.route;

import com.test.route.model.Stocktrading;
import org.apache.camel.Exchange;

public class StockProcess {
    public void updateVIP(Exchange exchange) throws Exception{
        Stocktrading stocktrading = exchange.getIn().getBody(Stocktrading.class);
        stocktrading.setCost("99999");
    }
    public void updateNonVIP(Exchange exchange) throws Exception{
        Stocktrading stocktrading = exchange.getIn().getBody(Stocktrading.class);
        stocktrading.setCost("111");
    }
}
