package com.test.route;

import org.apache.camel.builder.RouteBuilder;

public class ProcessRoute  extends RouteBuilder {

    @Override
    public void configure() {
        from("direct:start")
                .id("freemaker-test-route")
                .to("freemaker:template/Request.ftl");
    }
}
