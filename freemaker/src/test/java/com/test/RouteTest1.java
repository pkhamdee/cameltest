package com.test;

import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.model.RouteDefinition;
import org.apache.camel.test.blueprint.CamelBlueprintTestSupport;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

public class RouteTest1 extends CamelBlueprintTestSupport {

    private static final Logger LOGGER = LoggerFactory.getLogger(RouteTest1.class);

    private static final String BLUEPRINT_DESCRIPTOR = "OSGI-INF/blueprint/blueprint.xml";

    @Override
    protected String getBlueprintDescriptor() {
        return BLUEPRINT_DESCRIPTOR;
    }


    @Test
    public void testReadFile_whenFileExist_thenPrintContent() throws Exception {
        final RouteDefinition route = context.getRouteDefinition("freemaker-test-route");

        context.start();

        getMockEndpoint("mock:output").expectedMessageCount(1);
        assertMockEndpointsSatisfied();
    }
}
