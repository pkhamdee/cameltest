package com.test.bean;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;

import java.util.Map;

public class MessageProcessor implements Processor {

    public void process(Exchange exchange) throws Exception {
        Map<String,Object> headers = exchange.getIn().getHeaders();

        for(String headerName : headers.keySet()){
            System.out.println("headerName:{"+headerName+"]");
        }

        Message originalMessage = exchange.getUnitOfWork().getOriginalInMessage();
        System.out.println("Original Message:{"+originalMessage+"]");
        exchange.getOut().setHeader("SchoolNum", exchange.getIn().getBody());
        exchange.getOut().setBody(originalMessage.getBody());

    }

}
