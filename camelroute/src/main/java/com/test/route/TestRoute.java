package com.test.route;

import com.test.bean.MessageBean;
import com.test.bean.MessageProcessor;
import org.apache.camel.builder.RouteBuilder;
import static org.apache.camel.LoggingLevel.INFO;

public class TestRoute extends RouteBuilder {

    private MessageProcessor messageProcessor;
    private MessageBean messageBean;

    @Override
    public void configure() throws Exception {
        from("{{TEST.FILE.ENDPOINT}}")
                .routeId("camelroute-2-route")
                .log(INFO,"com.test.route","${in.header.CamelFileName} with content -&gt; ${body}")
                .bean(messageBean,"printExchangeMsg")
                .process(messageProcessor)
                .setHeader("CamelFileName",simple("${date:now:yyyyMMddhhmmss}-read.xml"))
                .to("ftp://localhost:21/data/app/upload?username=jboss&password=jboss#1!");
    }

    public void setMessageBean(MessageBean messageBean) {
        this.messageBean = messageBean;
    }

    public void setMessageProcessor(MessageProcessor messageProcessor) {
        this.messageProcessor = messageProcessor;
    }
}
